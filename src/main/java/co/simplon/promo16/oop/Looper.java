package co.simplon.promo16.oop;

public class Looper {
    String phraseJava = "I love Java";

    public void love(int number) {
        for (int x = 1; x <= number; x++) {
            phraseJava += " very";

        }
        phraseJava += " much";
        // System.out.println(phraseJava);

    }

    public void multiTable(int multiply) {
        for (int i = 1; i <= 10; i++) {

            // System.out.println(multiply * i);
        }
    }

    public void multiTables() {
        for (int j = 1; j <= 10; j++) {
            for (int k = 1; k <= 10; k++) {
                 System.out.print(j * k + "\t");

            }
            // System.out.println();

        }

    }

    public void lineStars(int n) {
        for (int y = 0; y <= n; y++) {
             System.out.println("*");
        }
    }

    public void pyramid(int n) {
        for (int y = 0; y <= n; y++) {
            for (int z = 0; z <= y; z++) {
                System.out.print("*");
            }
             System.out.println();
        }

    }

}
