package co.simplon.promo16.oop;

public class Food {
    public String name;
    public int calories;
    public Food(String pasta, int calories) {
        this.name = pasta;
        this.calories = calories;
    }
}
