package co.simplon.promo16.oop;

import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes.Name;

import co.simplon.promo16.Person;

public class NameOrganiser {
    
    List<String> firstNamesList = new ArrayList<>(List.of("Yousra", "Phonevilay", "Florian", "Roland", "Melissa",
            "Florent", "Magatte", "Melwic", "Aïcha", "Oualid", "Claire", "Kevin", "Khaled", "Rafaël", "Nolwenn"));
    List<String> firstNamesStartingBy = new ArrayList<>();

    public void getNameStartingBy(String letter) {
        for (String firstName : firstNamesList) {
            if (firstName.startsWith(letter.toUpperCase())) {
                firstNamesStartingBy.add(firstName);
                System.out.println(firstNamesStartingBy);
            }
        }
    }

    public void oddOnly() {
        for (int i = 1; i <= firstNamesList.size(); i += 2) {
            System.out.println(firstNamesList.get(i));
        }
        // for (int i = 0; i < firstNamesList.size(); i++ ) {
        //     if (i % 2 != 0) {
        //         System.out.println(firstNamesList.get(i));
        //     }
        // }
    }
    public void Students() {
        for (int i = 0; i<firstNamesList.size(); i++) {
            String name = firstNamesList.get(i);
            if (i == 0 || i == firstNamesList.size()-1) {
                System.out.println(name.toUpperCase());
            } else {
                System.out.println(name);
            }
        }
    }
    public List<Person> promoMaker() {
        List<Person>promoList = new ArrayList<>();
        for (String name : firstNamesList) {
           Person person = new Person("p16", name);
        
           promoList.add(person);
        }
        return promoList;
    }

}
