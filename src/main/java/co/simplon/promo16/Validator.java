package co.simplon.promo16;

import java.util.regex.Pattern;

public class Validator {
    public int min;
    public int max;

    public boolean isCorrectLength(String phrase) {

        if (phrase.length() >= 2 && phrase.length() <= 32) {
            return true;
        } else {
            return false;
            
        }
    }
    public boolean haveTrailingSpaces(String phrase) {
        if (phrase.startsWith(" ") || phrase.endsWith(" ")) {
            return true;
        } else {
            return false;
        }
    }
    public boolean haveSpecialChars(String phrase) {
        if (phrase.contains("!") || phrase.contains("-") || phrase.contains("_") ) {
            return true;
        } else {
            return false;
        }
    
    }
    public boolean Validate(String phrase) {
        if (isCorrectLength(phrase) && !haveTrailingSpaces(phrase) && !haveSpecialChars(phrase) ) {
            return true;
        } else {
            return false;
        }
    }
    public boolean isCorrectPassword(String phrase) {
        final Pattern textPattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+$");
        return phrase.length() >= 4 && phrase.matches(".*\\d.*") && textPattern.matcher(phrase).matches();
    }
    
}
