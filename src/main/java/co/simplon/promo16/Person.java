package co.simplon.promo16;

import co.simplon.promo16.oop.Food;

public class Person {
    String name;
    String firstName;
    int energy = 100;

    public Person(String name, String firstName) {
        this.name = name;
        this.firstName = firstName;
    }
    public String toString() {
        return "This is a person. Name: " + name + " First name:" + firstName + " Energy:" +energy;

    }
    public void sleep() {
        energy += 40;
        if (energy > 100) {
            energy = 100;
        }
    }
    public void socialize(Person person) {
        if (energy >= 15) {
            // System.out.println("Hello " + firstName + " how are you ? " + "Energy is: " + (energy-15));
        }
        if (energy <= 30){
            // System.out.println("I can't even");
        } else if (energy <= 40 && energy >= 70){
            System.out.println("I'm ok :/");
        } else if (energy >= 70) {
            // System.out.println("I'm fine");
        }
        
    }

    public void answer(String question) {
        // System.out.println();
    }
    public void eat(Food food){
        // System.out.println(name + " eats " + food.name + ". Energy is " + (food.calories/10));
    }

    
    
}
