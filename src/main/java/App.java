

import java.util.ArrayList;
import java.util.List;

import co.simplon.promo16.oop.Dog;
import co.simplon.promo16.oop.Food;
import co.simplon.promo16.oop.Looper;
import co.simplon.promo16.oop.NameOrganiser;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) {
        // Dog goodDog = new Dog("Meow", 3);
        // System.out.println("Hello, my name is " + goodDog.name + " & I am " +
        // goodDog.age + " years old");

        // Calculator calculator = new Calculator();
        // calculator.add(2, 2);

        // int result = calculator.calculate(10, 30, "+");
        // System.out.println(calculator.calculate(10, result, "+"));

        // boolean evenResult = calculator.isEven(10);
        // System.out.println(evenResult);
        // Validator validator = new Validator();
        // boolean correctLength = validator.isCorrectLength("Je suis une phrase");
        // System.out.println(correctLength);

        // boolean spaces = validator.haveTrailingSpaces("Not a phrase here");
        // System.out.println(spaces);

        // boolean characters = validator.haveSpecialChars("Hey this is phrase one");
        // System.out.println((characters));

        // boolean validate = validator.Validate("Is this phrase correct ? ");
        // System.out.println(validate);

        // Person person = new Person("Proust", "Marcel");
        // System.out.println(person.toString());
        // person.sleep();

        // Person othePerson = new Person("Kirk", "James T");
        // System.out.println(othePerson.toString());

        // Person personThree = new Person("Dupas", "Phonevilay");
        // System.out.println(personThree.toString());

        // person.socialize(personThree);
        // person.socialize(othePerson);

        // Looper loop = new Looper();
        // loop.love(100);

        // loop.multiTable(2);
        // loop.multiTables();

        // Food pasta = new Food("pasta", 400);

        // loop.lineStars(9);
        // loop.pyramid(5);

        // person.eat(pasta);

        // int age = 20;
        // int ageLimit = 18;

        // List<String> stringList = new ArrayList<>();
        // stringList.add("ë");
        // stringList.add("ehe");
        // stringList.add("binin");
        // stringList.add("ö");

        // for(int i=0; i < stringList.size(); i++) {
        //     System.out.println(stringList.get(i));
        // }

        // for (String string : stringList) {
        //     System.out.println(string);
        // }

        // if (age >= ageLimit) {
            // System.out.println(true);

        // }
        // String myName = "Phonevilay";
        // if (myName.contains("vilay")) {
            // System.out.println("Bonjour, " + myName);
        // } else {
            // System.out.println("Casse-toi");

        // }
        // if (myName.length() <= ageLimit) {
            // System.out.println("Name is shorter");
        // }
        // int number = 3;
        // if (number % 2 == 0) {
            // System.out.println("Even");
        // } else {
            // System.out.println("Odd");
        // }
    }

}
